# дз 1

def from_keyboard_y_or_n():
    """
    Function asks Y or N from user's keyboard
    :return: boolean value (True if 'Y', False if 'N')
    """
    while True:
        answer = input('введите (Y/N): ').upper()

        if answer in {'Y', 'N'}:
            break
        else:
            print('Y или N')

    result = True if answer == 'Y' else False

    return result


# дз 2

def year_word(year):
    """
    Function returns a string 'год' or 'года' or 'лет' depending on the receiving year
    :param year: int
    :return: return a word
    """
    if type(year) == int:
        if str(year)[-2:] == '11' or str(year)[-1] == '0' or int(str(year)[-1]) in range(5, 10) or int(str(year)[-2:]) \
                in range(12, 15):
            return 'лет'
        if str(year)[-1] == '1' and year != 11:
            return 'год'
        else:
            return 'года'


def handle_age(year):
    """
    Function receives a year and return a string
    :param year: int
    :return: a string that depends on the year
    """
    if type(year) != int or year not in range(1, 120):
        return 'не понимаю'
    else:
        year_string = year_word(year)

        if year < 7:
            return f'Тебе {year} {year_string}, где твои мама и папа?'
        elif year < 18:
            return f'Тебе {year} {year_string}, а мы не продаем сигареты несовершеннолетним.'
        elif year > 65:
            return f'Вам уже {year} {year_string}, вы в зоне риска!'
        else:
            return f'Наденьте маску, вам же {year} {year_string}!'


def conclusion_by_age():
    """
    Function asks the age of user and returns a corresponding conclusion
    :return: a string
    """
    try:
        age = int(input('сколько Вам лет? '))
    except ValueError:
        age = None

    return handle_age(age)
