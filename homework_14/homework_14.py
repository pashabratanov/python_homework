import re
import requests
from datetime import datetime

# Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ), отримайте теперішній курс валют
# и запишіть його в TXT-файл в такому форматі:
#  "[дата, на яку актуальний курс]"
# 1. [назва валюти 1] to UAH: [значення курсу валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу валюти n]
#
#
# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс


class UahCurrency:
    currencies_list = None
    date = None
    __url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date='
    handled_list = []

    def __init__(self, date=None):
        if type(date) == str and not re.search(r'^\d{8}$', date):
            raise TypeError(f'date must has format \'yyyymmdd\'')
        self.date = datetime.now().strftime("%Y%m%d") if not date else date

    def save(self):
        """
        The method is used to retrieve uah currency, process it and save to file
        :return:
        """
        self.__retrieve_currencies_list()

        if 'message' in self.currencies_list[0].keys():
            print(self.currencies_list[0]['message'])
        else:
            self.__process_currencies()

    def __retrieve_currencies_list(self):
        """
        The method is used to make request, and convert it to json
        :return:
        """
        res = Request(self.__url + self.date + '&json').get()
        if res.status_code == 200 and res.headers.get('Content-Type', None) == 'application/json; charset=utf-8':
            try:
                self.currencies_list = res.json()
            except:
                pass

    def __process_currencies(self):
        """
        the method is used to validate and handle content of currencies_list and save it to the file.
        :return:
        """
        if self.currencies_list:
            exchange_date = self.currencies_list[0].get('exchangedate', None)
            if exchange_date:
                self.handled_list.append(exchange_date + '\n')
                n = 1
                for item in self.currencies_list:
                    self.handled_list.append(CurrencyString(item, n).build())
                    n += 1
                Saving(self.handled_list).perform()


class Request:
    url = None

    def __init__(self, url):
        self.url = url

    def get(self):
        """
        The method is used to perform get request and return response
        :return: Response object
        """
        try:
            response = requests.get(self.url)
        except Exception as e:
            print(e)
        else:
            return response


class Saving:
    data = None
    filename = None

    def __init__(self, data, filename='currency.txt'):
        if type(data) != list:
            raise TypeError(f'{data} must be list')
        self.data = data
        if type(filename) != str:
            raise TypeError(f'{filename} must be string')
        self.filename = filename

    def perform(self):
        """
        The method is used to save list strings to file
        :return:
        """
        with open(self.filename, 'w') as file:
            for item in self.data:
                file.write(str(item))


class CurrencyString:
    dct = None

    def __init__(self, dct, number):
        if type(dct) != dict:
            raise TypeError(f'{dct} must be dict')
        self.dct = dct
        if type(number) != int:
            raise TypeError(f'{number} must be int')
        self.number = number

    def build(self):
        """
        The method is used to build and return string with name and value
        :return: str
        """
        name = self.dct.get('txt', None)
        value = self.dct.get('rate', None)

        if name and value:
            return f'{self.number} {name} to UAH: {value}\n'


uah = UahCurrency('20220101').save()