# Rock Scissors Paper
from random import choice as random_choice
from datetime import datetime

# Доработайте игру с занятия следующим образом: добавьте новые игровые фигуры (камень ножницы, бумага -> камень,
# ножницы, бумага, ящерица, Спок) https://www.youtube.com/watch?v=A2BYHWFpgVQ . Также игра должна записывать данные
# статистики в текстовый файл (пронумерованный список: дата и время, кто выиграл, что выбросили). Запись в файл
# https://docs.python.org/3/library/functions.html#open или https://pythonworld.ru/tipy-dannyx-v-python/fajly-rabota-s-fajlami.html


def user_choice(*variants):
    """
    The function asks user to type one of the variants and returns it
    :param variants: iterable object (list, tuple)
    :return: str
    """
    msg = f'Choose one of: ({", ".join(variants)}): \n'
    while True:
        user_input = input(msg).capitalize()
        if user_input not in variants:
            print('Wrong!')
        else:
            return user_input


def computer_choice(*variants):
    """
    The function performs random choice (one element) from variants (list, tuple) and returns it
    :param variants: iterable object(list, tuple)
    :return: element of variants
    """
    return random_choice(variants)


def is_user_win(rules_to_win, user_figure, computer_figure):
    """
    The function analizes user_figure and computer_figure according to rules_to_win
    :param rules_to_win:
    :param user_figure:
    :param computer_figure:
    :return:
    """
    if user_figure == computer_figure:
        return

    if computer_figure in rules_to_win[user_figure]:
        return True
    else:
        return False


def make_message(result, user_figure, computer_figure):
    """
    The function returns string that includes: result of game (victory or draw), user figure and computer figure
    :param result: True, False or None
    :param user_figure: str
    :param computer_figure: str
    :return: str
    """
    dct = {
        True: 'User win!',
        False: 'Computer win!',
        None: 'Draw!',
    }
    return f'User - \'{user_figure}\', computer - \'{computer_figure}\', result is: {dct[result]}'


def log_index(filename):
    """
    The function returns the new index based on the count of lines in logfile, if file doesn't exist returns 1
    :param filename: str
    :return: int
    """
    try:
        with open(filename) as f:
            return len(list(f)) + 1
    except FileNotFoundError:
        return 1


def save_to_log(message, filename='game.log'):
    """
    The function saves data to filename file
    :param filename: str
    :param message: str
    :return: None
    """
    data = f'{log_index(filename)} {datetime.now()} {message}'
    with open(filename, 'a') as f:
        f.write(data + '\n')


def rspls_game():
    """
    The function launches (Rock, Scissors, Paper, Lizard, Spock) game, saves the result to log, and returns result to
    user
    :return: str
    """
    rules_to_win = {
        'Rock': {'Scissors', 'Lizard'},
        'Scissors': {'Paper', 'Lizard'},
        'Paper': {'Rock', 'Spock'},
        'Lizard': {'Spock', 'Paper'},
        'Spock': {'Scissors', 'Rock'}
    }

    user_figure = user_choice(*rules_to_win.keys())
    computer_figure = computer_choice(*rules_to_win.keys())
    result = is_user_win(rules_to_win, user_figure, computer_figure)
    message = make_message(result, user_figure, computer_figure)
    save_to_log(message)
    return message


print(rspls_game())
