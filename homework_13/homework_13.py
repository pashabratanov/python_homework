import re

# Напишіть функцію (хто може - клас) яка приймає довільний текст і знаходить в ньому всі номерні знаки України і
# повертає їх у вигляді списку.
#
# Стандарти номерних знаків
#
# AB1234CD
# 12 345-67 AB
# a12345BC


class LicensePlate:
    parsed_text = None

    __rules = (
            r'[A-Z]{2}\d{4}[A-Z]{2}',
            r'[0-9]{2} \d{3}-\d{2} [A-Z]{2}',
            r'[a-z]\d{5}[A-Z]{2}'
        )

    def __init__(self, parsed_text):
        if type(parsed_text) != str:
            raise TypeError('The attribute must be string')
        if parsed_text == '':
            raise TypeError('The string can not be empty')
        self.parsed_text = parsed_text

    @property
    def list(self):
        """
        This method is used to find all licence plates that corresponds rules and returns them
        :return: list
        """
        license_plates = []

        for rule in self.__rules:
            license_plates += re.findall(rule, self.parsed_text)
        return license_plates

    def __str__(self):
        return f'The text contains: {self.list}'

    def __bool__(self):
        return len(self.list) > 0
