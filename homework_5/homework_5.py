# Написать функцию, принимающую два аргумента. Функция должна:
# a. - если оба аргумента относятся к числовым типам - вернуть их произведение,
# b. - если к строкам - соединить в одну строку и вернуть,
# c. - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# d. в любом другом случае вернуть кортеж (tuple) из аргументов


# performs some operation that depends on the type of each argument, and returns corresponding result
def arguments_handler(arg1, arg2):
    if (type(arg1) == float or type(arg1) == int) and (type(arg2) == float or type(arg2) == int):
        return arg1 * arg2
    elif type(arg1) == str and type(arg2) == str:
        return arg1 + arg2
    elif type(arg1) == str and not type(arg2) != str:
        return {arg1: arg2}
    else:
        return arg1, arg2


# Пользователь вводит строку произвольной длины. Написать функцию, которая принимает строку и должна вернуть словарь
# следующего содержания:
# a. ключ - количество букв в слове, значение - list слов с таким количеством букв.
# b. отдельным ключем, например "0", записать количество пробелов.
# c. отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:

# {
#   "0": количество пробелов в строке
#   "1": list слов из одной буквы
#   "2": list слов из двух букв
#   "3": list слов из трех букв
#   и т.д ...
#   "punctuation" : tuple уникальных знаков препинания
# }


# asks a user to write something (it can't be empty), and returns it as a string
def get_str_from_keyboard():
    while True:
        incoming_str = input('Please, write a string:')
        if len(incoming_str) > 0:
            return incoming_str
        else:
            continue


# returns the count of all characters except symbols of punctuation
def count_of_letters(word):
    punctuation_count = 0
    for char in word:
        if char in '.?/,\':;-!()':
            punctuation_count += 1
    return len(word) - punctuation_count


# group words by length and return it as a dict: #  {'1': ['o'], '2': ['do'], etc}
def group_words_by_length(incoming_string):
    words = incoming_string.split()

    length_set = {count_of_letters(word) for word in words}

    my_dict = {str(i): [] for i in length_set}

    for word in words:
        length_word = str(count_of_letters(word))
        my_dict[length_word].append(word)

    return my_dict


# return a tuple of uniq punctuation signs
def tuple_of_uniq_signs(incoming_string):
    signs_set = set()
    for char in incoming_string:
        if char in '.?/,\':;-!()':
            signs_set.add(char)
        else:
            continue
    return tuple(signs_set)


# return a dict that contains information about string: count of spaces, punctuation signs and group words by length
def string_dictionary(incoming_str):
    if type(incoming_str) == str and len(incoming_str) > 0:
        result = {'0': incoming_str.count(' ')}
        result.update(group_words_by_length(incoming_str))
        result.update({'punctuation': tuple_of_uniq_signs(incoming_str)})
        return result


my_str = get_str_from_keyboard()
print(string_dictionary(my_str))
