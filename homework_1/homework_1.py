# Задача 2: Создать две переменные first=10, second=30. Вывести на экран
# результат математического взаимодействия (+, -, *, / и тд.) для этих чисел.

first = 10
second = 30
print('Task 2:')
print('first + second =', first + second)
print('first - second =', first - second)
print('first * second =', first * second)
print('first / second =', first / second)
print('first // second =', first // second)
print('first ** second =', first ** second)
print('first % second =', first % second)

# Задача 3: Создать переменную и поочередно записать в нее результат сравнения
# (<, > , ==, !=) чисел из задания 1. После этого вывести на экран значение
# каждой полученной переменной.

print('\nTask 3:')
res = first < second
print(first, '<', second, res)

res = first > second
print(first, '>', second, res)

res = first == second
print(first, '==', second, res)

res = first != second
print(first, '!=', second, res)

# Задача 4: Создать переменную - результат конкатенации (сложения) строк str1="Hello "
# и str2="world". Вывести на ее экран.
str1 = 'Hello '
str2 = 'world'

concatenation_result = str1 + str2
print('\nTask 4:')
print('Concatenation of two strings:', concatenation_result)
