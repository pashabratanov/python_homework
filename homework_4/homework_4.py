# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово,
# в котором присутствуют подряд две гласные буквы.

# retrieve a string that has, at least, two words
while True:
    string = input('Please, enter a string: ')
    if len(string) > 0:
        words = set(string.split())
        if len(words) > 1:
            break
        else:
            print('write at least 2 different words')

with_two_vowels_in_row = []

# add  to 'with_two_vowels_in_row' list
for word in words:
    first_vowel_index = None
    second_vowel_index = None
    word_length = len(word)
    char_id = 0
    while char_id < word_length:
        if word[char_id].lower() in {'a', 'e', 'i', 'o', 'u', 'y'}:  # if symbol is vowel
            if first_vowel_index is None and second_vowel_index is None:
                first_vowel_index = char_id
            elif first_vowel_index is not None and second_vowel_index is None:
                second_vowel_index = char_id
                if second_vowel_index - first_vowel_index < 2:
                    with_two_vowels_in_row.append(word)
                    break
                else:
                    first_vowel_index = second_vowel_index
                    second_vowel_index = None
        char_id += 1

# finding the shortest word(s)
if with_two_vowels_in_row:
    length_list = [len(word) for word in with_two_vowels_in_row]

    length_list.sort()
    shortest_length = length_list[0]  # the shortest length between words

    result = 'the shortest word(s) with two vowels in row: '

    for word in with_two_vowels_in_row:
        if len(word) == shortest_length:
            result += f'{word} '
else:
    result = 'no word with two vowels in row:-)'

print(result)
# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных
# продавцов: { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
# "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}. Написать код, который найдет и выведет на
# экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой. Например:
# lower_limit = 35.9
# upper_limit = 37.3
# > match: "g-store", "royal-service"

while True:
    try:
        lower_limit = float(input('Please, enter a lower limit: '))
    except ValueError:
        print('it must be digits')
    else:
        break

while True:
    try:
        upper_limit = float(input('Please, enter an upper limit: '))
    except ValueError:
        print('it must be digits')
    else:
        if lower_limit < upper_limit:
            break
        else:
            print('\'upper limit\' can not be smaller than \'lower limit\'')

sellers = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}

names = set()

for seller, price in sellers.items():
    if (price >= lower_limit) and (price <= upper_limit):
        names.add(seller)

print(f'lower_limit = {lower_limit}')
print(f'upper_limit = {upper_limit}')

if len(names) == 0:
    print('no match :-(')
else:
    print(f'> match: {names}')

