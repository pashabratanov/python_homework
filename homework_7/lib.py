from random import randint


def clue_for_player(searched_number, user_number):
    """
    The function returns a clue (string) that depends on the difference between searched_number and user_number
    :param searched_number: int
    :param user_number: int
    :return: str
    """
    if searched_number != user_number:
        clue = 'Холодно'

        difference = max(searched_number, user_number) - min(searched_number, user_number)

        if difference in range(5, 11):
            clue = 'Тепло'
        elif difference in range(1, 5):
            clue = 'Горячо'
        return clue


def ask_a_number(lower_limit, upper_limit):
    """
    Function asks to type any number between lower_limit and upper_limit, and returns it
    :param lower_limit: int
    :param upper_limit: int
    :return: int
    """
    while True:
        try:
            number = int(input('введите число: '))
        except ValueError:
            number = 0

        if number in range(lower_limit, upper_limit + 1):
            return number
        else:
            print(f'от {lower_limit} до {upper_limit}')


def guess_a_number_game(lower_limit, upper_limit):
    """
    The function starts the game: user must guess the number between lower_limit and upper_limit and stops
    if user win.
    :param lower_limit: int
    :param upper_limit: int
    :return:
    """
    random_number = randint(lower_limit, upper_limit)

    while True:
        user_number = ask_a_number(lower_limit, upper_limit)

        if random_number == user_number:
            print('Поздравляем! Вы выграли!')
            break
        else:
            print(f'вы не угадали, "{clue_for_player(random_number, user_number)}"')


def play_again():
    """
    The function asks a question, answer must be 'Y' or 'N' (allowed lower case).
    Then it converts it to boolean (if Y - True, if N - False)
    :return: boolean (True or False)
    """
    while True:
        answer = input('повторить игру (Y/N)? ').upper()

        if answer in {'Y', 'N'}:
            break
        else:
            print('Y или N')

    answer = True if answer == 'Y' else False

    return answer


def play_game():
    """
    The function start a guess game, continue depends on a user
    :return: None
    """

    while True:
        print('Новая игра! Компьютер сгенерировал число от 1 до 100. Вам нужно его угадать...')
        guess_a_number_game(1, 100)

        if not play_again():
            break

    print('До встречи! Спасибо за игру!')
