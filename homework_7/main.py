from lib import play_game

# Пишем игру. Программа выбирает из диапазона чисел (пусть для начала будет 1-100) случайное число и предлагает
# пользователю его угадать. Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать
# еще раз, пока он не угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить.
# N - Прекратить. Опционально - добавьте в задание вывод сообщения-подсказки. Если пользователь ввел число,
# и не угадал - сообщать: "Холодно" если разница между загаданным и введенным числами больше 10, "Тепло" -
# если от 5 до 10 и "Горячо" если от 4 до 1.

# П.С. У вас уже есть знание функций так что все выполнение можно разбить на функции. Постарайтесь чтобы функции
# выполняли только одну задачу. Не используйте рекурсию.
# Случайными значениями занимается модуль random (https://docs.python.org/3/library/random.html)

play_game()