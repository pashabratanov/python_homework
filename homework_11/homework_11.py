from time import time

# Доработайте классы Point и Line из занятия. Обеспечьте передачу в first_point и second_point класса Line только
# объектов класса Point с помощью property


class Point:
    def __init__(self, coord_x, coord_y):
        if not isinstance(coord_x, (int, float)) or not isinstance(coord_y, (int, float)):
            raise TypeError('It must be int or float')
        self.__coords = coord_x, coord_y

    @property
    def x(self):
        return self.__coords[0]

    @property
    def y(self):
        return self.__coords[1]

    def __str__(self):
        return f'Point: {self.__coords}'

    def __add__(self, other):
        return Line(self, other)


class Line:
    def __init__(self, begin, end):
        self.first_point = begin
        self.second_point = end

    @property
    def first_point(self):
        return self.__first_point

    @first_point.setter
    def first_point(self, value):
        if type(value) is not Point:
            raise TypeError(f'\'{value}\' must belong to Point class')
        self.__first_point = value

    @property
    def second_point(self):
        return self.__second_point

    @second_point.setter
    def second_point(self, value):
        if type(value) is not Point:
            raise TypeError(f'\'{value}\' must belong to Point class')
        self.__second_point = value

    @property
    def length(self):
        """
        this property calculates and returns the length of imagine line between first_point and second_point
        :return: float
        """
        x = (self.first_point.x - self.second_point.x) ** 2
        y = (self.first_point.y - self.second_point.y) ** 2
        return (x + y) ** 0.5

    def __str__(self):
        return f'The line consists of two points: {self.first_point} and {self.second_point}'


# Напишите декоратор, который замеряет и принтует время выполнения функции


def time_decorator(decorated_func):
    """
    The function adds the ability of printing the proceeding time (in seconds) of incoming decorated_func
    :param decorated_func:
    :return: function
    """
    def wrapper(*args, **kwargs):
        start_time = time()
        res = decorated_func(*args, **kwargs)
        print(f'proceeded in {round(time() - start_time, 3)} seconds.')
        return res
    return wrapper


@time_decorator
def calculate_fibonacci(n):
    """
    The function wraps the call to 'fibonacci_rec' function to avoid unnecessary time_decorator's printing of processing
     time as the 'fibonacci_rec' is recursive function
    :param n: int
    :return: int
    """
    return fibonacci_rec(n)


def fibonacci_rec(n):
    """
    The function recursively calculates returns n-th number of Fibonacci sequence
    :param n: int
    :return: int
    """
    if n == 1:
        return 0
    if n in (2, 3):
        return 1
    return fibonacci_rec(n - 2) + fibonacci_rec(n - 1)
