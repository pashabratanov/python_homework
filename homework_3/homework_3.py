# 1. Сформировать строку, в которой содержится информация определенном слове в строке. Например "The [номер] symbol in
# [тут слово] is [значение символа по номеру в слове]". Слово и номер получите с помощью input() или воспользуйтесь
# константой. Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".

while True:
    word = input('Please, enter a word: ')

    length_of_word = len(word)

    if length_of_word > 0:
        break
    else:
        print('it can\'t be empty')

while True:
    try:
        number = int(input(f'number (1-{length_of_word}): '))
    except ValueError:
        print('it can be only digits')
        continue

    if (number > 0) and (number <= length_of_word):
        break
    else:
        print('wrong number')

character = word[number - 1]
info_str = f'The {number}th symbol in \'{word}\' is \'{character}\'.'
print(info_str)

# 2. Ввести из консоли строку. Определить количество слов в этой строке, которые заканчиваются на букву "o" (учтите, что
# буквы бывают заглавными).

while True:
    string = input('Пожалуйста, введите строку: ')
    if len(string) > 0:
        break

words_from_string = string.lower().split()
ends_with_o_count = 0

for word in words_from_string:
    if (word[-1] == 'o') or (word[-1] == 'о'):  # last letter is Latin 'o' or Cyrillic 'o' character?
        ends_with_o_count += 1

result = f'Количество слов, которые заканчиваются буквой \'o\' или \'О\': {ends_with_o_count}.'

print(result)

# 3. Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишите
# механизм, который cформирует новый list (например lst2), который содержит только переменные типа str, которые есть в
# lst1.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for element in lst1:
    if type(element) == str:
        lst2.append(element)

print('Исходный list с данными:', lst1)
print('Элементы типа \'str\':', lst2)

