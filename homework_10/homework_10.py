# Создайте класс "Транспортное средство" и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль".
# Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты для "Самолет", "Автомобиль", "Корабль".
# В наследниках реализуйте характерные для них атрибуты и методы

class Vehicle:
    passengers_count = 0
    made_in = 'China'

    def set_passengers_count(self, new_count):
        """
        The function sets new count of passengers
        :param new_count: int
        :return: None
        """
        self.passengers_count = new_count

    def set_made_in(self, new_country):
        """
        The function sets country of production
        :param new_country: str
        :return: None
        """
        self.made_in = new_country


class Car(Vehicle):
    passengers_count = 3
    full_self_driven = False
    mileage = 150_000

    def set_full_self_driven(self, boolean):
        """
        The function sets is this car full self-driven
        :param boolean: True or False
        :return: None
        """
        self.full_self_driven = boolean

    def set_mileage(self, new_mileage):
        """
        The function sets the mileage of car
        :param new_mileage: int
        :return: None
        """
        self.mileage = new_mileage

    def specs(self):
        msg = f'Count of passengers: {self.passengers_count}, made_in: {self.made_in}, ' \
              f'full self-driven: {self.full_self_driven}, mileage: {self.mileage} km.'
        return msg


class Ship(Vehicle):
    flag = 'Panama'
    passengers_count = 15

    def set_flag(self, new_flag):
        """
        The function sets the flag of the ship (country)
        :param new_flag: str
        :return: None
        """
        self.flag = new_flag

    def specs(self):
        msg = f'Count of passengers: {self.passengers_count}, made in {self.made_in}, flag: {self.flag}.'
        return msg


class Plain(Vehicle):
    count_of_engines = 4
    passengers_count = 350
    range_of_flight = 5000

    def set_count_of_engines(self, new_count):
        """
        The function sets plain's count of engines
        :param new_count: int
        :return: None
        """
        self.count_of_engines = new_count

    def set_range_of_flight(self, new_range):
        """
        The function sets plain's max range of flight (km)
        :param new_range: int
        :return: None
        """
        self.range_of_flight = new_range

    def specs(self):
        msg = f'Count of passengers: {self.passengers_count}, made_in {self.made_in}, ' \
              f'count of engines: {self.count_of_engines}, range of flight: {self.range_of_flight}.'
        return msg
